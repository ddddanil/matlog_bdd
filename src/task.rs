use biodivine_lib_bdd as bdd;
use bitvec::prelude::*;
use itertools::{izip, Itertools};
use std::{
    any::{type_name, TypeId},
    collections::{hash_map::Entry, BTreeMap, HashMap},
    fmt::Debug,
    num::NonZeroUsize,
};
use tracing::info;

use crate::{
    errors::{LabError, Result},
    objects::{ObjectField, ObjectId},
    properties::Property,
    relationship::Relationship,
};

type TypeName = &'static str;

#[derive(Debug)]
pub struct PropertyBuilder {
    objects: ObjectField,
    properties: HashMap<TypeId, (TypeName, NonZeroUsize)>,
}

impl PropertyBuilder {
    pub fn new(field: ObjectField) -> Self {
        Self {
            objects: field,
            properties: Default::default(),
        }
    }

    pub fn with_property<P: Property + 'static>(mut self) -> Result<Self> {
        let type_id = TypeId::of::<P>();
        let type_name = type_name::<P>();
        let var_count = NonZeroUsize::try_from(P::COUNT)
            .map_err(|_| LabError::PropertyHasNoValues(type_name))?;
        match self.properties.entry(type_id) {
            Entry::Vacant(entry) => entry.insert((type_name, var_count)),
            Entry::Occupied(_) => Err(LabError::PropertyAlreadyRegistered(type_name))?,
        };
        Ok(self)
    }

    pub fn build(self) -> Result<(RelationshipBuilder, ResultDecoder)> {
        let Self {
            objects,
            properties,
        } = self;
        let mut variables = bdd::BddVariableSetBuilder::new();
        objects
            .all_coordinates()
            .map(|coord| objects.get_id(&coord))
            .map_ok(|object| {
                properties.values().for_each(|(name, var_count)| {
                    let bit_count = (usize::from(var_count.clone()) as f64).log2().ceil() as usize;
                    (0..bit_count).into_iter().for_each(|i| {
                        let var_name = format!("{}_{}_{}", object, name, i);
                        variables.make_variable(&var_name);
                    })
                })
            })
            .try_collect()?;
        let variables = variables.build();
        let task_variables = TaskVariables {
            variables: variables.clone(),
        };
        let const_true = task_variables.variables.mk_true();
        let builder = RelationshipBuilder {
            objects: objects.clone(),
            variables: task_variables,
            bdd: const_true,
        };
        let decoder = ResultDecoder {
            objects,
            known_variables: variables,
        };
        Ok((builder, decoder))
    }
}

pub struct TaskVariables {
    variables: bdd::BddVariableSet,
}

impl TaskVariables {
    pub fn get<P: Property>(&self, object: ObjectId, property: P) -> bdd::Bdd {
        let prop_name = type_name::<P>();
        let prop_no: u8 = property.into();
        let bit_count = (P::COUNT as f64).log2().ceil() as usize;
        prop_no.view_bits::<Lsb0>()[..bit_count]
            .into_iter()
            .enumerate()
            .map(move |(i, b)| {
                let var_name = format!("{}_{}_{}", object, prop_name, i);
                match b.as_ref() {
                    true => self.variables.mk_var_by_name(&var_name),
                    false => self.variables.mk_not_var_by_name(&var_name),
                }
            })
            .reduce(|acc, var| acc.and(&var))
            .expect("Property is not empty")
    }
}

impl Debug for TaskVariables {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_set()
            .entries(self.variables.variables().iter())
            .finish()
    }
}

#[derive(Debug)]
pub struct RelationshipBuilder {
    objects: ObjectField,
    variables: TaskVariables,
    bdd: bdd::Bdd,
}

impl RelationshipBuilder {
    pub fn with_relationship<R: Relationship>(mut self, relationship: R) -> Result<Self> {
        let additional_expr = relationship.get_expression(&self.objects, &self.variables)?;
        self.bdd = self.bdd.and(&additional_expr);
        info!(
            cardinality = self.bdd.cardinality(),
            "Added relationship {}",
            type_name::<R>()
        );
        Ok(self)
    }

    pub fn build(self) -> (bdd::BddVariableSet, bdd::Bdd) {
        let Self {
            variables: TaskVariables { variables },
            bdd,
            ..
        } = self;
        (variables, bdd)
    }
}

pub struct ResultDecoder {
    known_variables: bdd::BddVariableSet,
    objects: ObjectField,
}

impl ResultDecoder {
    pub fn decode_property<P: Property>(
        &self,
        valuation: &bdd::BddValuation,
    ) -> Result<BTreeMap<ObjectId, P>> {
        let prop_name = type_name::<P>();
        let bit_count = (P::COUNT as f64).log2().ceil() as usize;
        self.objects
            .all_ids()
            .map(|object| {
                let bits: Vec<bool> = (0..bit_count)
                    .map(|i| {
                        let var_name = format!("{}_{}_{}", object, prop_name, i);
                        let var = self.known_variables.mk_var_by_name(&var_name);
                        var.eval_in(&valuation)
                    })
                    .collect();
                let mut discriminant = 0u8;
                izip!(discriminant.view_bits_mut::<Lsb0>(), bits.into_iter())
                    .for_each(|(mut bit, var)| *bit = var);
                let prop =
                    P::try_from(discriminant).map_err(|_| LabError::IncorrectDiscriminant {
                        typename: prop_name,
                        variants: P::COUNT,
                        discriminant,
                    })?;
                Ok((object, prop))
            })
            .collect()
    }
}
