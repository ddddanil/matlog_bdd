use thiserror::Error;

use crate::objects::ObjectCoordinate;

#[derive(Debug, Error)]
pub enum LabError {
    #[error("Invalid coordinate {coordinate} in {size}x{size} grid")]
    InvalidCoordinate {
        size: usize,
        coordinate: ObjectCoordinate,
    },
    #[error("Property is already registered: {0}")]
    PropertyAlreadyRegistered(&'static str),
    #[error("Property has no values: {0}")]
    PropertyHasNoValues(&'static str),
    #[error("Invalid discriminant for property {typename} ({variants}): {discriminant}")]
    IncorrectDiscriminant {
        typename: &'static str,
        variants: usize,
        discriminant: u8,
    },
}

pub type Result<T> = std::result::Result<T, LabError>;
