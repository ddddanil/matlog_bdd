use std::marker::PhantomData;

use biodivine_lib_bdd as bdd;
use biodivine_lib_bdd::bdd;

use crate::{
    errors::Result,
    objects::{AdjDirection, DirectionPair, ObjectCoordinate, ObjectField},
    properties::Property,
    task::TaskVariables,
};

pub trait Relationship {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd>;
}

struct UniquePropertyValue<P: Property> {
    _p: PhantomData<P>,
}

pub fn unique_property_values<P: Property>() -> impl Relationship {
    UniquePropertyValue {
        _p: PhantomData::<P>::default(),
    }
}

impl<P: Property> Relationship for UniquePropertyValue<P> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let expr = objects
            .all_ids()
            .flat_map(|object| P::iter().map(move |prop1| (object, prop1)))
            .flat_map(|(object, prop1)| P::iter().map(move |prop2| (object, prop1, prop2)))
            .flat_map(|(object, prop1, prop2)| {
                if prop1 != prop2 {
                    let prop1 = variables.get(object, prop1);
                    let prop2 = variables.get(object, prop2);
                    Some(bdd!(prop1 => (!prop2)))
                } else {
                    None
                }
            })
            .reduce(|acc, var| acc.and(&var))
            .expect("Property is not empty");
        Ok(expr)
    }
}

struct DefinedProperty<P: Property> {
    _p: PhantomData<P>,
}

pub fn defined_property<P: Property>() -> impl Relationship {
    DefinedProperty {
        _p: PhantomData::<P>::default(),
    }
}

impl<P: Property> Relationship for DefinedProperty<P> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let expr = objects
            .all_ids()
            .flat_map(|object| P::iter().map(move |prop| (object, prop)))
            .map(|(object, prop)| variables.get(object, prop))
            .reduce(|acc, var| acc.or(&var))
            .expect("Property is not empty");
        Ok(expr)
    }
}

struct SetProperty<P: Property> {
    object: ObjectCoordinate,
    property: P,
}

pub fn set_property<P: Property>(object: ObjectCoordinate, property: P) -> impl Relationship {
    SetProperty { object, property }
}

impl<P: Property> Relationship for SetProperty<P> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let object_id = objects.get_id(&self.object)?;
        Ok(variables.get(object_id, self.property))
    }
}

struct PropertiesTogether<P1: Property, P2: Property> {
    a: P1,
    b: P2,
}

pub fn properties_are_together<P1: Property, P2: Property>(
    property_a: P1,
    property_b: P2,
) -> impl Relationship {
    PropertiesTogether {
        a: property_a,
        b: property_b,
    }
}

impl<P1: Property, P2: Property> Relationship for PropertiesTogether<P1, P2> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let expr = objects
            .all_ids()
            .map(|id| {
                let prop_a = variables.get(id, self.a);
                let prop_b = variables.get(id, self.b);
                bdd!(prop_a <=> prop_b)
            })
            .reduce(|acc, var| acc.and(&var))
            .expect("Property is not empty");
        Ok(expr)
    }
}

struct DirectedNeighbour<PFr: Property, PTo: Property> {
    direction: AdjDirection,
    prop_from: PFr,
    prop_to: PTo,
}

pub fn neighbour_to<PFr: Property, PTo: Property>(prop_from: PFr, dir: AdjDirection, prop_to: PTo) -> impl Relationship {
    DirectedNeighbour {
        direction: dir,
        prop_from,
        prop_to,
    }
}

impl<PFr: Property, PTo: Property> Relationship for DirectedNeighbour<PFr, PTo> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let with_neighbours = objects
            .all_coordinates()
            .flat_map(|from| {
                objects
                    .get_neighbour(&from, &self.direction)
                    .map(|to| (from, to))
            })
            .map(|(from, to)| {
                (
                    objects.get_id(&from).expect("ID from all_coordinates"),
                    objects.get_id(&to).expect("ID from get_neighbour"),
                )
            })
            .map(|(from, to)| {
                let prop_from = variables.get(from, self.prop_from);
                let prop_to = variables.get(to, self.prop_to);
                bdd!(prop_from <=> prop_to)
            });
        let without_neighbours = objects
            .objects_without_neighbour(&self.direction)
            .map(|from| objects.get_id(&from).expect("ID from without neighbour"))
            .map(|from| {
                let prop_from = variables.get(from, self.prop_from);
                bdd!(!prop_from)
            });
        let expr = with_neighbours
            .chain(without_neighbours)
            .reduce(|acc, var| acc.and(&var))
            .expect("Property is not empty");
        Ok(expr)
    }
}

struct MultiDirectedNeighbour<PFr: Property, PTo: Property> {
    directions: DirectionPair,
    prop_from: PFr,
    prop_to: PTo,
}

impl<PFr: Property, PTo: Property> Relationship for MultiDirectedNeighbour<PFr, PTo> {
    fn get_expression(self, objects: &ObjectField, variables: &TaskVariables) -> Result<bdd::Bdd> {
        let expr = objects
            .all_coordinates()
            .flat_map(|from| {
                objects
                    .get_neighbour(&from, &self.directions.left)
                    .map(|left| (from, left))
            })
            .flat_map(|(from, left)| {
                objects
                    .get_neighbour(&from, &self.directions.right)
                    .map(|right| (from, left, right))
            })
            .map(|(from, left, right)| {
                (
                    objects.get_id(&from).expect("ID from all_coordinates"),
                    objects.get_id(&left).expect("ID from get_neighbour"),
                    objects.get_id(&right).expect("ID from get_neighbour"),
                )
            })
            .map(|(from, left, right)| {
                let prop_from = variables.get(from, self.prop_from);
                let prop_left = variables.get(left, self.prop_to);
                let prop_right = variables.get(right, self.prop_to);
                bdd!((prop_from <=> prop_left) | (prop_from <=> prop_right))
            })
            .reduce(|acc, var| acc.and(&var))
            .expect("Property is not empty");
        Ok(expr)
    }
}

pub fn to_left<PFr: Property, PTo: Property>(
    dirs: &DirectionPair,
    prop_from: PFr,
    prop_to: PTo,
) -> impl Relationship {
    DirectedNeighbour {
        direction: dirs.left,
        prop_from,
        prop_to,
    }
}

pub fn to_right<PFr: Property, PTo: Property>(
    dirs: &DirectionPair,
    prop_from: PFr,
    prop_to: PTo,
) -> impl Relationship {
    DirectedNeighbour {
        direction: dirs.right,
        prop_from,
        prop_to,
    }
}

pub fn next_to<PFr: Property, PTo: Property>(
    dirs: &DirectionPair,
    prop_from: PFr,
    prop_to: PTo,
) -> impl Relationship {
    MultiDirectedNeighbour {
        directions: *dirs,
        prop_from,
        prop_to,
    }
}
