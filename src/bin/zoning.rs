use color_eyre::{eyre::eyre, Result};
use course::{
    defined_property, next_to, properties_are_together, set_property, to_left, to_right,
    unique_property_values, AdjDirection, DirectionPair, ObjectField, Property, PropertyBuilder,
    RelationshipBuilder,
};
use itertools::izip;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use strum::{Display, EnumCount, EnumIter, EnumVariantNames, IntoEnumIterator};

fn setup() -> color_eyre::Result<()> {
    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    use tracing_error::ErrorLayer;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;
    use tracing_tree::HierarchicalLayer;
    tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(
            HierarchicalLayer::new(2)
                .with_targets(true)
                .with_bracketed_fields(true),
        )
        .with(ErrorLayer::default())
        .init();
    Ok(())
}

// Properties

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum Color {
    Red,
    Green,
    Blue,
    Cyan,
    Magenta,
    Yellow,
    White,
    Black,
    Beige,
}

impl Property for Color {}

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum ZoneType {
    ResidentialSparce,
    ResidentialMedium,
    ResidentialDense,
    CommercialSparce,
    CommercialMedium,
    CommercialDense,
    IndustrialSparce,
    IndustrialMedium,
    IndustrialDense,
}

impl Property for ZoneType {}

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum PlotArea {
    Awkward,
    Tiny,
    Small,
    Constrained,
    Comfortable,
    Large,
    Extreme,
    Monumental,
    Park,
}

impl Property for PlotArea {}

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum Special {
    BeachFront,
    Historical,
    Mountainous,
    DangerousSoil,
    HighCrime,
    TouristSpot,
    BadRoadAccess,
    PublicTransport,
    CityLimits,
}

impl Property for Special {}

// Properties

fn props_set(
    dirs: DirectionPair,
    objects: &ObjectField,
    mut builder: RelationshipBuilder,
) -> course::Result<RelationshipBuilder> {
    for (coord, color, zone, plot, special) in izip!(
        objects.all_coordinates(),
        Color::iter(),
        ZoneType::iter(),
        PlotArea::iter(),
        Special::iter()
    ) {
        builder = builder
            .with_relationship(set_property(coord, color))?
            .with_relationship(set_property(coord, zone))?
            .with_relationship(set_property(coord, plot))?
            .with_relationship(set_property(coord, special))?;
    }
    Ok(builder)
}

fn props1(
    dirs: DirectionPair,
    builder: RelationshipBuilder,
) -> course::Result<RelationshipBuilder> {
    let builder = builder
        // Type 1
        .with_relationship(set_property((0, 0).into(), Color::Magenta))?
        .with_relationship(set_property((0, 1).into(), PlotArea::Comfortable))?
        .with_relationship(set_property((1, 0).into(), Special::CityLimits))?
        .with_relationship(set_property((1, 0).into(), PlotArea::Awkward))?
        .with_relationship(set_property((2, 1).into(), PlotArea::Park))?
        .with_relationship(set_property((2, 2).into(), Special::BeachFront))?
        .with_relationship(set_property((2, 2).into(), ZoneType::CommercialMedium))?
        // Type 2
        .with_relationship(properties_are_together(
            ZoneType::CommercialMedium,
            Color::Cyan,
        ))?
        .with_relationship(properties_are_together(
            PlotArea::Park,
            ZoneType::CommercialSparce,
        ))?
        .with_relationship(properties_are_together(
            ZoneType::CommercialSparce,
            Special::HighCrime,
        ))?
        .with_relationship(properties_are_together(
            ZoneType::ResidentialDense,
            Color::Black,
        ))?
        // Type 3
        .with_relationship(to_right(&dirs, Color::Magenta, Color::Black))?
        .with_relationship(to_right(&dirs, PlotArea::Comfortable, Color::Yellow))?
        .with_relationship(to_right(&dirs, ZoneType::CommercialSparce, PlotArea::Extreme))?
        // Type 4
        .with_relationship(next_to(
            &dirs,
            ZoneType::CommercialDense,
            ZoneType::ResidentialDense,
        ))?
        .with_relationship(next_to(
            &dirs,
            Color::Black,
            Special::Historical,
        ))?
        .with_relationship(next_to(
            &dirs,
            PlotArea::Comfortable,
            ZoneType::IndustrialSparce,
        ))?
        ;
        // .with_relationship(next_to(&dirs, Special::HighCrime, PlotArea::Tiny))?;
    Ok(builder)
}

fn main() -> Result<()> {
    setup()?;
    let field = ObjectField::new(3.try_into().unwrap(), true, false);
    let dirs = DirectionPair {
        left: AdjDirection::NW,
        right: AdjDirection::SE,
    };
    let (relationship_builder, results) = PropertyBuilder::new(field.clone())
        .with_property::<Color>()?
        .with_property::<ZoneType>()?
        .with_property::<PlotArea>()?
        .with_property::<Special>()?
        .build()?;
    let builder = relationship_builder
        .with_relationship(unique_property_values::<Color>())?
        .with_relationship(unique_property_values::<ZoneType>())?
        .with_relationship(unique_property_values::<PlotArea>())?
        .with_relationship(unique_property_values::<Special>())?
        .with_relationship(defined_property::<Color>())?
        .with_relationship(defined_property::<ZoneType>())?
        .with_relationship(defined_property::<PlotArea>())?
        .with_relationship(defined_property::<Special>())?;
    // let (variables, bdd) = props_set(dirs, &field, builder)?.build();
    let (variables, bdd) = props1(dirs, builder)?.build();
    // let s = bdd.to_dot_string(&variables, true);
    // println!("{s}");
    println!("Cardinality: {}", bdd.cardinality());
    // print!("Most free clause: ");
    // bdd.most_free_clause()
    //     .ok_or(eyre!("Empty bdd"))?
    //     .to_values()
    //     .into_iter()
    //     .for_each(|(var, val)| print!("{}={:5} ", variables.name_of(var), val));
    if bdd.cardinality() < 20.0 {
        println!("Valuations: {}", bdd.sat_valuations().count());
        for val in bdd.sat_valuations() {
            let colors = results.decode_property::<Color>(&val)?;
            print!("{:>10}:  ", "Colors");
            for (obj, col) in colors.into_iter() {
                print!("{}: {:18} ", obj, col);
            }
            println!();
            let zone_types = results.decode_property::<ZoneType>(&val)?;
            print!("{:>10}:  ", "ZoneTypes");
            for (obj, col) in zone_types.into_iter() {
                print!("{}: {:18} ", obj, col);
            }
            println!();
            let plot_areas = results.decode_property::<PlotArea>(&val)?;
            print!("{:>10}:  ", "PlotAreas");
            for (obj, col) in plot_areas.into_iter() {
                print!("{}: {:18} ", obj, col);
            }
            println!();
            let specials = results.decode_property::<Special>(&val)?;
            print!("{:>10}:  ", "Specials");
            for (obj, col) in specials.into_iter() {
                print!("{}: {:18} ", obj, col);
            }
            println!();
        }
    }
    Ok(())
}
