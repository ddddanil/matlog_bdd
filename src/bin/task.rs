use color_eyre::Result;
use course::{
    defined_property, neighbour_to, properties_are_together, set_property, unique_property_values,
    AdjDirection, ObjectField, Property, PropertyBuilder,
};
use num_enum::{IntoPrimitive, TryFromPrimitive};
use strum::{Display, EnumCount, EnumIter, EnumVariantNames};

// Properties

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum Color {
    RED,
    GREEN,
    BLUE,
    WHITE,
    BLACK,
}

impl Property for Color {}

#[derive(
    Debug,
    Display,
    Clone,
    Copy,
    PartialEq,
    Eq,
    EnumCount,
    EnumVariantNames,
    EnumIter,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
enum Size {
    SMALL,
    MEDIUM,
    LARGE,
}

impl Property for Size {}
// Properties

fn main() -> Result<()> {
    let field = ObjectField::new(2.try_into().unwrap(), false, false);
    let (relationship_builder, results) = PropertyBuilder::new(field.clone())
        .with_property::<Color>()?
        .with_property::<Size>()?
        .build()?;
    let (variables, bdd) = relationship_builder
        .with_relationship(unique_property_values::<Color>())?
        .with_relationship(unique_property_values::<Size>())?
        .with_relationship(defined_property::<Color>())?
        .with_relationship(defined_property::<Size>())?
        .with_relationship(set_property((0, 0).into(), Color::RED))?
        // .with_relationship(set_property((0, 1).into(), Color::GREEN))?
        // .with_relationship(set_property((1, 0).into(), Color::BLACK))?
        .with_relationship(set_property((1, 1).into(), Color::WHITE))?
        .with_relationship(set_property((0, 0).into(), Size::SMALL))?
        .with_relationship(set_property((0, 1).into(), Size::MEDIUM))?
        .with_relationship(set_property((1, 0).into(), Size::LARGE))?
        .with_relationship(set_property((1, 1).into(), Size::MEDIUM))?
        .with_relationship(properties_are_together(Color::BLACK, Size::LARGE))?
        .with_relationship(neighbour_to(Color::BLACK, AdjDirection::SW, Color::GREEN))?
        .build();
    // let s = bdd.to_dot_string(&variables, true);
    // println!("{s}");
    println!("Cardinality: {}", bdd.cardinality());
    for val in bdd.sat_valuations() {
        let colors = results.decode_property::<Color>(&val)?;
        print!("{:>10}:  ", "Colors");
        for (obj, col) in colors.into_iter() {
            print!("{}: {:15} ", obj, col);
        }
        println!();
        let sizes = results.decode_property::<Size>(&val)?;
        print!("{:>10}:  ", "Sizes");
        for (obj, col) in sizes.into_iter() {
            print!("{}: {:15} ", obj, col);
        }
        println!();
    }
    Ok(())
}
