use itertools::Itertools;

use crate::errors::{LabError, Result};
use std::{fmt::Display, num::NonZeroUsize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ObjectId(usize);

impl Display for ObjectId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ObjectCoordinate {
    pub x: usize,
    pub y: usize,
}

impl From<(usize, usize)> for ObjectCoordinate {
    fn from(value: (usize, usize)) -> Self {
        Self {
            x: value.0,
            y: value.1,
        }
    }
}

impl Display for ObjectCoordinate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({},{})", self.x, self.y)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AdjDirection {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW,
}

#[derive(Debug, Clone, Copy)]
pub struct DirectionPair {
    pub left: AdjDirection,
    pub right: AdjDirection,
}

#[derive(Debug, Clone)]
pub struct ObjectField {
    size: NonZeroUsize,
    wrap_lr: bool,
    wrap_tb: bool,
}

impl ObjectField {
    pub fn new(size: NonZeroUsize, wrap_lr: bool, wrap_tb: bool) -> Self {
        Self {
            size,
            wrap_lr,
            wrap_tb,
        }
    }

    pub fn object_count(&self) -> usize {
        self.size.checked_pow(2).unwrap().into()
    }

    pub fn get_id(&self, coord: &ObjectCoordinate) -> Result<ObjectId> {
        if coord.x >= self.size.into() || coord.y >= self.size.into() {
            Err(LabError::InvalidCoordinate {
                size: self.size.into(),
                coordinate: coord.clone(),
            })?;
        };
        let id = usize::from(self.size) * coord.y + coord.x;
        Ok(ObjectId(id))
    }

    pub fn are_neighbours(
        &self,
        from: &ObjectCoordinate,
        to: &ObjectCoordinate,
    ) -> Option<AdjDirection> {
        let sz: isize = usize::from(self.size) as isize;
        let mut dx = to.x as isize - from.x as isize;
        let mut dy = to.y as isize - from.y as isize;
        if self.wrap_lr && dx.abs() == sz - 1 {
            dx /= 1 - sz;
        };
        if self.wrap_tb && dy.abs() == sz - 1 {
            dy /= 1 - sz;
        };
        match (dx, dy) {
            (0, 0) => None,
            (0, -1) => Some(AdjDirection::N),
            (1, -1) => Some(AdjDirection::NE),
            (1, 0) => Some(AdjDirection::E),
            (1, 1) => Some(AdjDirection::SE),
            (0, 1) => Some(AdjDirection::S),
            (-1, 1) => Some(AdjDirection::SW),
            (-1, 0) => Some(AdjDirection::W),
            (-1, -1) => Some(AdjDirection::NW),
            (_, _) => None,
        }
    }

    pub fn get_neighbour(
        &self,
        from: &ObjectCoordinate,
        dir: &AdjDirection,
    ) -> Option<ObjectCoordinate> {
        self.all_neighbours(from)
            .find(|(_, d)| d == dir)
            .map(|(coord, _)| coord)
    }

    pub fn all_coordinates<'a>(&'a self) -> impl Iterator<Item = ObjectCoordinate> + 'a {
        (0usize..self.size.into())
            .flat_map(|x| (0usize..self.size.into()).map(move |y| (x, y)))
            .map(|(x, y)| ObjectCoordinate { x, y })
    }

    pub fn all_ids<'a>(&'a self) -> impl Iterator<Item = ObjectId> + 'a {
        self.all_coordinates()
            .map(|coord| self.get_id(&coord).expect("ID from all-coordinates"))
    }

    pub fn all_neighbours<'a>(
        &'a self,
        coord: &'a ObjectCoordinate,
    ) -> impl Iterator<Item = (ObjectCoordinate, AdjDirection)> + 'a {
        self.all_coordinates()
            .filter_map(|n| self.are_neighbours(coord, &n).map(|dir| (n, dir)))
    }

    pub fn objects_without_neighbour<'a>(
        &'a self,
        dir: &'a AdjDirection,
    ) -> impl Iterator<Item = ObjectCoordinate> + 'a {
        self.all_coordinates()
            .map(|c| {
                let directions = self.all_neighbours(&c).map(|(_, dir)| dir).collect_vec();
                (c, directions)
            })
            .filter(|(_, dirs)| !dirs.contains(dir))
            .map(|(c, _)| c)
    }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::{AdjDirection, ObjectCoordinate, ObjectField};

    #[test]
    fn get_all_neighbours_from_center() {
        let field = ObjectField {
            size: 3.try_into().unwrap(),
            wrap_lr: false,
            wrap_tb: false,
        };
        let center = ObjectCoordinate { x: 1, y: 1 };
        let manual_neighbours: Option<Vec<ObjectCoordinate>> = [
            field.get_neighbour(&center, &AdjDirection::N),
            field.get_neighbour(&center, &AdjDirection::NE),
            field.get_neighbour(&center, &AdjDirection::E),
            field.get_neighbour(&center, &AdjDirection::SE),
            field.get_neighbour(&center, &AdjDirection::S),
            field.get_neighbour(&center, &AdjDirection::SW),
            field.get_neighbour(&center, &AdjDirection::W),
            field.get_neighbour(&center, &AdjDirection::NW),
        ]
        .into_iter()
        .collect();
        let manual_neighbours = manual_neighbours.expect("NO failed neighbours");
        let auto_neighbours: Vec<ObjectCoordinate> =
            field.all_neighbours(&center).map(|(c, _)| c).collect();
        assert_that!(manual_neighbours.iter()).contains_all_of(&auto_neighbours.iter())
    }

    #[test]
    fn get_all_neighbours_with_wrap() {
        let field = ObjectField {
            size: 3.try_into().unwrap(),
            wrap_lr: true,
            wrap_tb: false,
        };
        let center = ObjectCoordinate { x: 0, y: 1 };
        let manual_neighbours: Option<Vec<ObjectCoordinate>> = [
            field.get_neighbour(&center, &AdjDirection::N),
            field.get_neighbour(&center, &AdjDirection::NE),
            field.get_neighbour(&center, &AdjDirection::E),
            field.get_neighbour(&center, &AdjDirection::SE),
            field.get_neighbour(&center, &AdjDirection::S),
            field.get_neighbour(&center, &AdjDirection::SW),
            field.get_neighbour(&center, &AdjDirection::W),
            field.get_neighbour(&center, &AdjDirection::NW),
        ]
        .into_iter()
        .collect();
        let manual_neighbours = manual_neighbours.expect("NO failed neighbours");
        let auto_neighbours: Vec<ObjectCoordinate> =
            field.all_neighbours(&center).map(|(c, _)| c).collect();
        assert_that!(manual_neighbours.iter()).contains_all_of(&auto_neighbours.iter())
    }
}
