use strum::{EnumCount, IntoEnumIterator, VariantNames};

pub trait Property:
    Eq + Copy + EnumCount + VariantNames + IntoEnumIterator + Into<u8> + TryFrom<u8>
{
}
