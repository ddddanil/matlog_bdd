// v (25, 1, 2)
// v1 = 25 -- соседи NW, SE
// v2 = 1 -- лев-прав границы
// v3 = 2 -- 7 4 3 4 (n1, n2, n3, n4)

mod errors;
mod objects;
mod properties;
mod relationship;
mod task;

pub use errors::{LabError, Result};
pub use objects::{AdjDirection, DirectionPair, ObjectCoordinate, ObjectField, ObjectId};
pub use properties::Property;
pub use relationship::{
    defined_property, next_to, properties_are_together, set_property, to_left, to_right,
    unique_property_values, neighbour_to,
};
pub use task::{PropertyBuilder, RelationshipBuilder};
